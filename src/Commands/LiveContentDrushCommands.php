<?php

namespace Drupal\live_content\Commands;

use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\live_content\FeedServer;
use Drush\Commands\DrushCommands;

/**
 * Class LiveContentDrushCommands.
 *
 * Drush commands for Live Content module.
 */
class LiveContentDrushCommands extends DrushCommands {

  /**
   * Config form settings for live content module.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $liveContentConfig;

  /**
   * LiveContentDrushCommands constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory for retrieving required config objects.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    parent::__construct();
    $this->liveContentConfig = $configFactory->get('live_content.settings');
  }

  /**
   * Load the container to get config factory.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container interface to load our service.
   *
   * @return \Drupal\live_content\Commands\LiveContentDrushCommands
   *   The config factory.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Starts Rachet websocket server.
   *
   * @command live-content-start-server
   * @aliases lc-ss
   * @usage drush live-content-start-server
   *   Starts Rachet websocket server.
   */
  public function start() {
    $port = $this->liveContentConfig->get('ws_server_port');
    $this->output()->writeln('Server now running on localhost:' . $port);
    FeedServer::start();
  }

}
