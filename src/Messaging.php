<?php

namespace Drupal\live_content;

use Drupal\Component\Utility\Xss;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides a messaging interface.
 */
class Messaging implements MessageComponentInterface {

  /**
   * Array object of clients.
   *
   * @var \SplObjectStorage
   */
  protected $clients;

  use StringTranslationTrait;

  /**
   * Constructs a new messaging object.
   */
  public function __construct() {
    $this->clients = new \SplObjectStorage();
  }

  /**
   * {@inheritdoc}
   */
  public function onOpen(ConnectionInterface $conn) {
    $this->clients->attach($conn);
    echo $this->t('New connection from: @id', ['@id' => "$conn->resourceId"]) . PHP_EOL;
  }

  /**
   * {@inheritdoc}
   */
  public function onMessage(ConnectionInterface $conn, $msg) {
    foreach ($this->clients as $client) {
      $client->send(Xss::filterAdmin($msg));
    }
    echo $this->t('New message received from: @$conn', ['$conn' => "$conn->resourceId"]) . PHP_EOL;
  }

  /**
   * {@inheritdoc}
   */
  public function onClose(ConnectionInterface $conn) {
    $this->clients->detach($conn);
    echo $this->t('Connection @id has disconnected', ['@id' => "$conn->resourceId"]) . PHP_EOL;
  }

  /**
   * {@inheritdoc}
   */
  public function onError(ConnectionInterface $conn, \Exception $e) {
    \Drupal::logger('live_content')
      ->error('Messaging error: @error ', ['@error' => $e->getMessage()]);
    echo $this->t('An error occurred: @error', ['@error' => $e->getMessage()]) . PHP_EOL;

    $conn->close();
  }

}
