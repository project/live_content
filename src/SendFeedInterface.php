<?php

namespace Drupal\live_content;

/**
 * Interface SendFeedInterface.
 *
 * An interface for sending live feeds to a server.
 */
interface SendFeedInterface {

  /**
   * Send a string message to a server.
   *
   * @param string $feed
   *   The feed message to send.
   */
  public function send($feed);

}
