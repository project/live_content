<?php

namespace Drupal\live_content;

use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;

/**
 * Class FeedServer.
 *
 * Inits Rachet WebSocket server.
 */
class FeedServer {

  /**
   * Starts WebSocket server.
   */
  public static function start() {
    // Load Server config.
    $config = \Drupal::config('live_content.settings');
    $port = $config->get('ws_server_port');

    // Init.
    $server = IoServer::factory(
      new HttpServer(
        new WsServer(
          new Messaging()
        )
      ),
      $port
    );
    $server->run();
    // Loop has stopped when this line executes.
    echo t('Server has stopped running..');
  }

}
