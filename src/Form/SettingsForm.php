<?php

namespace Drupal\live_content\Form;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;

/**
 * Class for live content settings form definition.
 */
class SettingsForm extends ConfigFormBase {

  // Max allowed port is 16-bit unsigned integer.
  const MAX_ALLOWED_PORT = 65535;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $configFactory, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configFactory);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'live_content.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'live_content_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('live_content.settings');

    // Populate content types for select options, if any.
    try {
      $node_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    }
    catch (InvalidPluginDefinitionException $e) {
    }
    catch (PluginNotFoundException $e) {
    }

    $node_type_options = [];
    if (!empty($node_types)) {
      foreach ($node_types as $node_type => $node_type_data) {
        $node_type_options[$node_type] = Html::escape($node_type_data->label());
      }
    }

    // Server settings.
    $form['server'] = [
      '#type' => 'fieldset',
      '#weight' => 0,
      '#title' => $this->t('Server settings'),
    ];
    $form['server']['ws_server_ip'] = [
      '#type' => 'textfield',
      '#title' => $this->t('WebSocket server IP address'),
      '#description' => $this->t('Enter IP address from your WebSocket server. This would be "127.0.0.1" for localhost.'),
      '#maxlength' => 15,
      '#size' => 32,
      '#required' => TRUE,
      '#default_value' => $config->get('ws_server_ip'),
    ];
    $form['server']['ws_server_port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('WebSocket server port'),
      '#description' => $this->t("Enter port number for your WebSocket server if needed, leave blank otherwise. Note, you need to use a port if you're planning to use Rachet WebSocket server."),
      '#maxlength' => 5,
      '#size' => 32,
      '#default_value' => $config->get('ws_server_port'),
    ];
    $form['server']['ws_server_protocol'] = [
      '#type' => 'select',
      '#title' => $this->t('WebSocket protocol'),
      '#description' => $this->t("Select your WebSocket's protocol depending on your server setup."),
      '#options' => [
        'ws' => 'WS',
        'wss' => 'WSS',
      ],
      '#required' => TRUE,
      '#default_value' => $config->get('ws_server_protocol'),
    ];

    // Feed settings.
    $form['feed'] = [
      '#type' => 'fieldset',
      '#weight' => 1,
      '#title' => $this->t('Live feed settings'),
    ];
    if (!empty($node_type_options)) {
      $form['feed']['node_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Select content type(s)'),
        '#description' => $this->t('Select content types that will push feed on new entries.'),
        '#options' => $node_type_options,
        '#size' => 5,
        '#multiple' => TRUE,
        '#default_value' => $config->get('node_type'),
      ];
    }
    $form['feed']['feed_limit'] = [
      '#type' => 'select',
      '#title' => $this->t('Enter feed limit'),
      '#description' => $this->t('Select number of items shown on live feed block.'),
      '#options' => [
        5 => 5,
        10 => 10,
        15 => 15,
        20 => 20,
      ],
      '#multiple' => FALSE,
      '#required' => TRUE,
      '#default_value' => $config->get('feed_limit'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $formIPAddress = trim($form_state->getValue('ws_server_ip'));
    $formPort = trim($form_state->getValue('ws_server_port'));
    // Validate IP address.
    if (!filter_var($formIPAddress, FILTER_VALIDATE_IP)) {
      $form_state->setErrorByName('ws_server_ip', 'Please enter a valid IP address.');
    }
    // Validate port entry.
    if (!is_numeric($formPort) && !empty($formPort)) {
      $form_state->setErrorByName('ws_server_port', $this->t('Please enter a valid port number.'));
    }
    elseif ($formPort > self::MAX_ALLOWED_PORT) {
      $form_state->setErrorByName('ws_server_port', $this->t('Port number cannot exceed max @port port number.', ['@port' => self::MAX_ALLOWED_PORT]));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('live_content.settings')
      ->set('ws_server_ip', trim($form_state->getValue('ws_server_ip')))
      ->set('ws_server_port', trim($form_state->getValue('ws_server_port')))
      ->set('ws_server_protocol', $form_state->getValue('ws_server_protocol'))
      ->set('feed_limit', $form_state->getValue('feed_limit'));
    // Make sure there were content types.
    if (is_array($form_state->getValue('node_type'))) {
      $this->config('live_content.settings')
        ->set('node_type', $form_state->getValue('node_type'));
    }
    $this->config('live_content.settings')->save();

    parent::submitForm($form, $form_state);
  }

}
