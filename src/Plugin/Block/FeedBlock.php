<?php

namespace Drupal\live_content\Plugin\Block;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Live content feed block.
 *
 * @Block (
 *   id = "live_content_feed_block",
 *   admin_label = @Translation("Live content feed"),
 * )
 */
class FeedBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Config form settings for live content module.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $liveContentConfig;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * FeedsBlock constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory for retrieving required config objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $configFactory, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->liveContentConfig = $configFactory->get('live_content.settings');
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get recent nodes, if any.
    try {
      $recentItems = $this->getRecentContent();
    }
    catch (InvalidPluginDefinitionException $e) {
    }
    catch (PluginNotFoundException $e) {
    }

    // Theme content titles as linked item list.
    if (!empty($recentItems)) {
      foreach ($recentItems as $itemNid => $itemTitle) {
        $items[] = Link::fromTextAndUrl($itemTitle, Url::fromUri('internal:/node/' . $itemNid));
      }
    }
    else {
      $items[] = $this->t('No content matching your settings was found');
    }

    return [
      '#theme' => 'live_content_feed_block',
      '#items' => $items,
      '#attached' => [
        'library' => ['live_content/live_content'],
        'drupalSettings' => [
          'live_content' => [
            'protocol' => $this->liveContentConfig->get('ws_server_protocol'),
            'serverIP' => $this->liveContentConfig->get('ws_server_ip'),
            'port' => $this->liveContentConfig->get('ws_server_port'),
          ],
        ],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Get recent content based on admin configuration settings.
   *
   * @return array
   *   Array of node titles keyed by their node id.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the storage handler couldn't be loaded.
   */
  public function getRecentContent() {
    $contentTypes = $this->liveContentConfig->get('node_type');
    $feedLimit = $this->liveContentConfig->get('feed_limit');
    $nodeTitles = [];

    // Query for recent nodes.
    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    if (!empty($contentTypes)) {
      $query->condition('type', $contentTypes, 'IN');
    }
    $query
      ->condition('status', TRUE)
      ->range(0, $feedLimit)
      ->sort('created', 'DESC');
    $nodeIDs = $query->execute();
    $nodes = $this->entityTypeManager->getStorage('node')
      ->loadMultiple($nodeIDs);

    // Index by node id.
    foreach ($nodes as $node) {
      $nodeTitles[$node->id()] = $node->getTitle();
    }

    return $nodeTitles;
  }

}
