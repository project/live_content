<?php

namespace Drupal\live_content;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Ratchet\Client;

/**
 * Sends a server feed to the client based on admin configuration settings.
 */
class Feed implements sendFeedInterface {

  use StringTranslationTrait;

  /**
   * Content types array.
   *
   * @var array
   */
  protected $contentTypes;

  /**
   * Feed message.
   *
   * @var string
   */
  protected $feed;

  /**
   * WebSocket server address.
   *
   * @var string
   */
  protected $serverIP;

  /**
   * WebSocket port number.
   *
   * @var string|null
   */
  protected $port;

  /**
   * WebSocket protocol.
   *
   * @var string
   */
  protected $protocol;

  /**
   * Constructs a new Feed object from settings.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory for retrieving required config objects.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $liveContentConfig = $configFactory->get('live_content.settings');
    $this->serverIP = $liveContentConfig->get('ws_server_ip');
    $this->port = $liveContentConfig->get('ws_server_port');
    $this->contentTypes = $liveContentConfig->get('node_type');
    $this->protocol = $liveContentConfig->get('ws_server_protocol');
  }

  /**
   * Get content types the feed is enabled on.
   *
   * @return array
   *   Array of content types machine names.
   */
  public function getContentTypes() {
    return $this->contentTypes ? $this->contentTypes : [];
  }

  /**
   * Get port number the server is listening on.
   *
   * @return int
   *   Port number.
   */
  public function getPort() {
    return $this->port;
  }

  /**
   * Sends a new item to the WebSocket server.
   *
   * @param string $feed
   *   Feed message to send to the client.
   *
   * @see https://github.com/ratchetphp/Pawl
   */
  public function send($feed) {
    $this->feed = $feed;
    // Connect to WebSocket and attempt to push message.
    Client\connect($this->protocol . '://' . $this->serverIP . ':' . $this->port)->then(function ($conn) {
      $conn->on('message', function ($msg) use ($conn) {
        echo $this->t('Received: @msg', ['@msg' => $msg]) . PHP_EOL;
        $conn->close();
      });

      $conn->send($this->feed);
    }, function ($e) {
      echo $this->t('Could not connect: @error', ['@error' => $e->getMessage()]) . PHP_EOL;
      \Drupal::logger('live_content')
        ->error('Connection error: @error ', ['@error' => $e->getMessage()]);
    });
  }

}
