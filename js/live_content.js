/**
 * @file
 * Attach behaviour for Live Content block.
 */
(function ($, Drupal, drupalSettings) {
  'use strict';
  // Init Websocket connection.
  const serverSettings = drupalSettings.live_content;
  const serverPort = serverSettings.port > 0 ? ':' + serverSettings.port : '';
  const wsConnection = new WebSocket(serverSettings.protocol + '://' + serverSettings.serverIP + serverPort);
  Drupal.behaviors.liveContent = {
    // Attach behaviour.
    attach: function (context) {
      let itemList = '#live-content-feed-items ul';
      wsConnection.onmessage = (e) => {
        // Add a new item from the feed while keeping same total.
        $(itemList + ' li:last', context).remove();
        $(itemList, context).prepend('<li>(' + Drupal.t('new') + ') ' + e.data + '</li>');
      };
      wsConnection.onerror = (e) => {
        Drupal.throwError(Drupal.t('WebSocket live content error: @error', {'@error': e.data}));
      };
    }
  };
})(jQuery, Drupal, drupalSettings);
