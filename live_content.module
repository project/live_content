<?php

/**
 * @file
 * Contains live_content.module.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function live_content_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the live content module.
    case 'help.page.live_content':
      $text = file_get_contents(dirname(__FILE__) . '/README.md');
      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . $text . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        return $filter->process($text, 'en');
      }
  }
  return NULL;
}

/**
 * Implements hook_entity_insert().
 */
function live_content_entity_insert(EntityInterface $entity) {
  // Get live content service.
  $live_content_service = \Drupal::service('live_content.feed');
  $live_content_content_types = $live_content_service->getContentTypes();
  if (
    !empty($live_content_content_types) &&
    $entity->getEntityType()->id() == "node" &&
    in_array($entity->bundle(), $live_content_content_types) &&
    $entity->isPublished()
  ) {
    // Push new content feed.
    $live_content_service->send($entity->toLink()->toString());
  }
}

/**
 * Implements hook_theme().
 */
function live_content_theme($existing, $type, $theme, $path) {
  return [
    'live_content_feed_block' => [
      'variables' => [
        'items' => [],
      ],
    ],
  ];
}
