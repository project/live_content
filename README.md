CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------

This module adds a block of recent titles that update in real time on new
content via a WebSocket. This functionality is implemented as a Drupal service,
so it can be invoked on any updates you want shown to the block. This feature
can potentially prove handy in content rich websites, like news, magazines or
for admins viewing various site events.

Available customization:

 * Filter which content types you want to see updates from
 * Limit the number of recent titles shown at a time in descending order
 * Enable more realtime events to the block with the supplied service

There are two components bundled into this module:
 
 * Run a WebSocket server with drush
 * Send asynchronous server updates to the client via the provided service

The WebSocket communication runs with the help of a third party library called
Rachet which needs some basic configuration in the module's settings to work.
 
 * To read more about Rachet and Pawl libraries visit their github page
 [here](https://github.com/ratchetphp/)
  also this demo and very helpful for Ratchet [site](http://socketo.me).

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/live_content

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/live_content


REQUIREMENTS
------------

 This module requires two external libraries besides drush:
 
  * [Rachet](https://github.com/ratchetphp/Ratchet), a PHP library for
  asynchronously serving WebSockets.
  * [Pawl](https://github.com/ratchetphp/Pawl), an Asynchronous WebSocket
  client.
  * Drush 9+ if you're planning to use Rachet WebSocket server.


INSTALLATION
------------

 * Install the Live content module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420
   for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Web services > Live content.
 
Server settings

The settings entered here affect communication between client and server.

 * The IP address will be used to send messages to your WebSocket
   server whether that is the Rachet server that you can run using drush or
   any other server you may have configured.

 * Rachet WebSocket server will by default bind on localhost and only
   allow connections by itself. You can additionally choose the protocol
   and port number to bind it on.

 * Pawl WebSocket client will use these settings to connect and push
   messages to the client.

Feed settings

 * Settings entered for the feeds block to filter the content types you want
   to feed from and and number of items to display.

 * Configure and enable the Live content feed block
   To enable the block go to Structure » Block layout » Place block button on
   the region you want and look for the "Live content feed" block then configure
   visibility and access rules as required.
    
Starting Rachet WebSocket server

 * To start the WebSocket server with drush type:
   "drush live-content-start-server" or "lc-ss" with drush alias and this
   should start the server in interactive mode. Alternatively run "lc-ss &" to
   run in the background.
   
Enabling the feed service on other events

 * In class context, you should inject the "live_content.feed" service and
   call it's send function wherever you want.
   In procedural context like a .module file, you first get the service from
   Drupal container then pass the message you want shown to the send function
   as per below:
```
    $live_content_service = \Drupal::service('live_content.feed');
    $live_content_service->send("Your message");
```
 It's also possible to enable "WSS" protocol for your WebSocket depending on
 your host's setup. For more information follow link below:
 https://groups.google.com/forum/#!msg/ratchet-php/dj-PgPPO_J0/k2hp7fvKdV8J.


TROUBLESHOOTING
---------------

  * No messages shown on block. Make sure there are no client errors in the
    browser console and the WebSocket server is receiving messages.

  * Server going down on production, install supervisord on your host to keep
    process alive.

  * Rachet troubleshooting page [here](http://socketo.me/docs/troubleshooting).


MAINTAINERS
-----------

 * seroton - https://www.drupal.org/u/seroton
